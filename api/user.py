from flask import jsonify,request,Response
from flask_restful import Resource, Api
from app import app
from database.user import User
from schemas.user import user_schema
import jwt
import datetime
from . import token_required
api = Api(app)
class UserAPI(Resource):
    api_url         = '/api/v1.0/user'
    api_endpoint    = 'user'

    @token_required
    def get(self,curr_user):
        user = user_schema.dump(curr_user)
        return jsonify(user.data)

    def post(self):
        req = request.get_json()
        user = User.add_user(request.get_json())
        token = jwt.encode({'public_id':user.public_id,'exp':datetime.datetime.utcnow()+datetime.timedelta(days=5)},app.config['SECRET_KEY']) # JWT access-token will be valid for 5 days
        return jsonify({'message':'New user created successfully.','token':token.decode('UTF-8')})
    
    @token_required
    def put(self, curr_user):
        data = request.get_json()
        if(data["password"] != data["confirm_password"]):
            resp = jsonify({'message':"Confirm password doesn't match."})
            resp.status_code = 400
            return resp
        user = User.update_user(data,curr_user)
        return jsonify({'message':'User details updated successfully.'})


api.add_resource(UserAPI, UserAPI.api_url, endpoint = UserAPI.api_endpoint)

