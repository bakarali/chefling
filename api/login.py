from flask import jsonify,request,Response
from flask_restful import Resource, Api
from app import app
from database.user import User
import jwt
import datetime

api = Api(app)
class LoginAPI(Resource):
    api_url         = '/api/v1.0/login'
    api_endpoint    = 'login'

    def post(self):
        auth  = request.authorization
        if not auth or not auth.username or not auth.password:
            return Response('Could not verify', 401, {'WWW-Authenticate':'Basic realm="Login Required"'})
        user = User.query.filter_by(email = auth.username,password = auth.password).first()
        if not user:
            return Response('Could not verify', 401, {'WWW-Authenticate':'Basic realm="Login Required"'})
        token = jwt.encode({'public_id':user.public_id,'exp':datetime.datetime.utcnow()+datetime.timedelta(days=5)},app.config['SECRET_KEY'])
        return jsonify({'token':token.decode('UTF-8')})
api.add_resource(LoginAPI, LoginAPI.api_url, endpoint = LoginAPI.api_endpoint)

