import jwt
from flask import jsonify,request
from database.user import User
from functools import wraps
from app import app

def  token_required(f):
    @wraps(f)
    def decorated(*args,**kwargs):
        token = {}
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        
        if not token:
            res = jsonify({"Message":"Token is missing"})
            res.status_code = 400
            return res
        try:
            data=jwt.decode(token,app.config['SECRET_KEY'])
            curr_user = User.query.filter(User.public_id == data['public_id']).first()
            kwargs['curr_user'] = curr_user
        except:
            res = jsonify({"Message":"Invalid token"})
            res.status_code = 400
            return res
        return f(*args,**kwargs)
    return decorated