**Instalation APP**
* 1: For install pip, virtualenv and dependency use command(```sh install.sh```)

**Migration DB**
* 1: For migrate db use command(```python manager.py db migrate```)

**Deploy DB**
* 1: For deploy db use command(```python manager.py db upgrade```)

**Run App**
* 1: For run the app (```python manager.py runserver```)

----------------------
**CURL Requests**

 - Create a user
``` curl -X POST \
  http://0.0.0.0:9000/api/v1.0/user \
  -H 'Content-Type: application/json' \
  -d '{
	"name":"Ali",
	"password":"bakar123",
	"email":"bakarali@gmail.com"
}'
```

- Login
```curl - bakarali@gmail.com:bakar123 -X POST \
  http://0.0.0.0:9000/api/v1.0/login \
  -H 'cache-control: no-cache'
```


- Get a user profile
```curl -X GET \
  http://0.0.0.0:9000/api/v1.0/user \
  -H 'x-access-token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwdWJsaWNfaWQiOiI4NmZiNWRhMy1hZmUzLTQxZWYtYjFiZS04Y2ZjNjE5Yzk4MDEiLCJleHAiOjE1NjkyMDQ2MTd9.63x5wlIsvzmMsRNS8ChJ2vzmFYvPfrhGOvFR0xKqAxg'
```

- Update user profile
```curl -X PUT \
  http://0.0.0.0:9000/api/v1.0/user \
  -H 'Content-Type: application/json' \
  -H 'x-access-token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwdWJsaWNfaWQiOiI4NmZiNWRhMy1hZmUzLTQxZWYtYjFiZS04Y2ZjNjE5Yzk4MDEiLCJleHAiOjE1NjkyMDQ2MTd9.63x5wlIsvzmMsRNS8ChJ2vzmFYvPfrhGOvFR0xKqAxg' \
  -d '{
	"name":"Ali",
	"password":"bakar123",
	"confirm_password":"bakar123",
	"email":"bakarali@gmail.com"
}'
```


**Note** : 
* To take care of the mobile application,  I have added versioning on API.
* I have used SQLite database for the assignment but for a real-time application, we can you Postgres or MySQL database.
