from flask_marshmallow import Marshmallow
from marshmallow import fields,validate
from app import app
from database.user import User
ma = Marshmallow(app)

class UserSchema(ma.Schema):
    class Meta:
        model = User
    id = fields.Integer()
    name = fields.String()
    email = fields.String()
    public_id = fields.String()
user_schema = UserSchema(many=False)
