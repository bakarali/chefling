from flask import Flask
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/sqlitedatabase.db'
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'fabb67b8-a999-11e9-a2a3-2a2ae2dbcce4'
