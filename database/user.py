from . import db
from uuid import uuid4

class User(db.Model):
    __tablename__   = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String())
    name = db.Column(db.String())
    password = db.Column(db.String())
    public_id =  db.Column(db.String())

    @staticmethod
    def add_user(data):
        new_user = User(public_id = str(uuid4()),name =data["name"],password=data["password"],email=data["email"])
        db.session.add(new_user)
        db.session.commit()
        return new_user

    @staticmethod
    def update_user(data, user):
        user.name = data["name"]
        user.password = data["password"]
        user.email = data["email"]
        db.session.commit()